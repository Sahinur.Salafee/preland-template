'use strict';

jQuery(window).on('scroll', function(){
      if(jQuery(window).scrollTop()){
        jQuery('header').addClass('fixed-it')
      }
      else{
        jQuery('header').removeClass('fixed-it')
      }
    })

    jQuery(window).on('scroll', function () {
        var docpos = $(document).scrollTop();

        if (2 < docpos) {
            $(".fixed-it").addClass('sticky');
        } else {
            $(".fixed-it").removeClass('sticky');
        }
    });

    $(".navbar-toggler").on('click', function() {
        $(".content-pt").toggleClass("blur-it");
        $("header").toggleClass("opacity-it");
    });


    $('.slider-wrapper').slick({
          dots: false,
          arrows:false,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 3000,
          variableWidth: false,
          fade: true,
          responsive: [
              {
                breakpoint: 1024,
                settings: {
                  arrows:false,
                  slidesToShow:1,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: true
                }
              },
              {
                breakpoint: 768,
                settings: {
                  arrows:false,
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 480,
                settings: {
                  arrows:false,
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
    });