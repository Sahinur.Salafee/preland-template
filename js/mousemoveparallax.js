 $.fn.parallax = function (resistance, mouse) {
     $el = $(this);
     TweenLite.to($el, 0.2, {
         x: -((mouse.clientX - (window.innerWidth / 2)) / resistance),
         y: -((mouse.clientY - (window.innerHeight / 2)) / resistance)
     });

 };
(function ($) {
        "use strict";
        $(document).mousemove(function (e) {
            $('.left-sideimages__one').parallax(-80, e);
            $('.left-sideimages__two').parallax(-30, e);
            $('.right-sideimages__one').parallax(-80, e);
            $('.right-sideimages__two').parallax(-40, e);
            $('.hero-feature-image').parallax(-80, e);
        });
}(jQuery));


